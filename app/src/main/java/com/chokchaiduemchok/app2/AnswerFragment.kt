package com.chokchaiduemchok.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import com.chokchaiduemchok.app2.databinding.FragmentAnswerBinding


class AnswerFragment : Fragment() {
    private var _binding: FragmentAnswerBinding? = null
    private val binding get()= _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       /* val view = inflater.inflate(R.layout.fragment_answer,container,false)
        val textView : TextView? = view?.findViewById(R.id.received_value_id)
        val args = this.arguments
        val inputData = args?.get("data")
        textView?.text = inputData.toString()
        return view*/

        _binding = FragmentAnswerBinding.inflate(inflater,container,false)
       // val textView : TextView? = view?.findViewById(R.id.received_value_id)
        //val args = this.arguments
        //val inputData = args?.get("data")
       // textView?.text = inputData.toString()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.buttonback?.setOnClickListener {
            val action = AnswerFragmentDirections.actionAnswerFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}