package com.chokchaiduemchok.app2

import android.os.Bundle
import android.provider.Settings.Global.putInt
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.compose.ui.platform.textInputServiceFactory
import androidx.navigation.findNavController
import com.chokchaiduemchok.app2.databinding.FragmentPlusBinding

class PlusFragment : Fragment() {

    private var _binding: FragmentPlusBinding? = null
    private val binding get()= _binding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPlusBinding.inflate(inflater,container,false)
        return binding?.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       binding?.buttonplusans?.setOnClickListener {

          //val input = " " + (binding?.num1?.text.toString().toInt()+binding?.num2?.text.toString().toInt())
         //  Toast.makeText(context,"Result"+input,Toast.LENGTH_LONG).show()

        val action = PlusFragmentDirections.actionPlusFragmentToAnswerFragment()
        view.findNavController().navigate(action)
       }
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}


